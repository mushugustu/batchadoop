# Spark Batch Training

This project contains the batch task used to train a predictive model
for the Retail Forecasting exercise.  

We use the grid-search and cross-validation techniques in order to determine 
the best model and the best set of hyperparameters for this application. 
For more information about machine-learning model tuning with Spark visit [this reference].

[this reference]: https://spark.apache.org/docs/2.2.0/ml-tuning.html

## Setup

**Dependencies**

* [Java 8]
    * Debain-based OS: `sudo apt install openjdk-8-jdk`
* [Scala Build Tool] (SBT)
    * Debian-based OS: 

```commandline
$ sudo apt install wget
$ wget https://dl.bintray.com/sbt/debian/sbt-1.2.6.deb
$ sudo dpkg -i sbt-1.2.6.deb
$ rm -r sbt-1.2.6.deb
$ sbt about
```

* [Spark] (optional)

[Scala Build Tool]: https://www.scala-sbt.org/1.x/docs/Setup.html
[Spark]: https://spark.apache.org/downloads.html
[Java 8]: https://www.oracle.com/technetwork/pt/java/javase/downloads/jdk8-downloads-2133151.html

## Usage

Consider tuning the configuration file located at `src/main/resources/application.conf`
according to your use-case and hardware limitations. 

### Model training

There are two ways of training the predictive model: 

1. **Compile and run as a Spark Job**: recommended for production environments
    * Use the `sbt assembly` command to generate a fatjar.
    * The resulting `jar` will be in the `target` directory. 
    * Submit the jar into your Spark engine.
2. **Compile and run using SBT**: recommended for development environment
    * Use the following command: `sbt run -mem 10240`
    * The `-mem` flag allows SBT to use more RAM (e.g. 10240 MB).

### Model serving

Having the PMML file of the trained model we can create a REST API using
[Docker] and the [Openscoring] web services. 

[Docker]: https://www.docker.com/
[Openscoring]: https://github.com/openscoring/openscoring

1. Use `docker` to run the Openscoring API on port `8080`
    * Build an image: `docker build -t retailforecasting/openscoring resources/provided/docker/`
    * Run the container: `docker container run -p 8080:8080 -d --name retailforecasting retailforecasting/openscoring`
2. Upload the model with the name `cumulative-weekly-predictions`.
    * `curl -X PUT --data-binary @resources/output/model/model-{score}.pmml -H "Content-type: text/xml" http://{id-address}:8080/openscoring/model/cumulative-weekly-predictions`
    * Replace `{score}` and `{ip-address}` as needed. 
3. Verify using a get request.
    * Visit in a browser: `http://{ip-address}:8080/openscoring/model`
    * Visit in a browser: `http://{ip-address}:8080/openscoring/model/cumulative-weekly-predictions`
4. Test some samples.
    * `curl -X POST --data-binary @resources/provided/requests-json/req-1.json -H "Content-type: application/json"  http://{ip-address}:8080/openscoring/model/cumulative-weekly-predictions`

## Results

This repo enables two important steps of real-time model serving:

* **A batch process** capable of generating the trained machine learning model information in PMML format.
* **A REST API** based on Openscoring used for model serving. 

So far, the training process needs to be manually executed by explicitly running 
an SBT task or by submitting a Spark Job. This approach can be automated by using 
an [Airflow] workflow capable of updating the sku-files, running the Spark Job and submitting the 
model into Openscoring. Moreover, several optimizations can be applied to the Dockerfile used to 
create the Openscoring image. Consider using [Kubernetes] to automate devops and clusterize the application. 

[Airflow]: https://airflow.apache.org/start.html
[Kubernetes]: https://kubernetes.io/

## Authors and contributions 
Add yourself when creating a PR to this project. 

* Initial work - [Rodrigo Hernández Mota](https://www.linkedin.com/in/rhdzmota/)

## License

TBD
