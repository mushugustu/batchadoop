package com.diplomadodb.retailforecasting.ml

import com.diplomadodb.retailforecasting.ml.regression.RandomForest
import org.apache.spark.ml.evaluation.RegressionEvaluator
import javax.xml.transform.stream.StreamResult
import org.apache.spark.ml.param.ParamMap
import org.apache.spark.ml.tuning.CrossValidator
import org.apache.spark.ml.{Pipeline, PipelineModel}
import org.apache.spark.sql.types.StructType
import org.apache.spark.sql.{Dataset, Row}
import org.dmg.pmml.PMML
import org.jpmml.model.JAXBUtil
import org.jpmml.sparkml.PMMLBuilder
import java.io.BufferedWriter
import java.nio.charset.StandardCharsets
import java.nio.file.{Files, Path, Paths, StandardOpenOption}

import com.diplomadodb.retailforecasting.config.Settings

object Model {

  object Implicits {
    implicit class PMMLModel(pipelineModel: PipelineModel) {
      val precision = 0.01
      val zeroThreshold = 1e-5
      def toPMML(schema: StructType, verificationData: Option[Dataset[Row]] = None): PMML = {
        val pmmlBuilder = new PMMLBuilder(schema, pipelineModel)
        if (verificationData.nonEmpty) pmmlBuilder.verify(verificationData.get, precision, zeroThreshold).build()
        else pmmlBuilder.build()
        pmmlBuilder.build()
      }
      def exportPMML(schema: StructType, score: Double, verificationData: Option[Dataset[Row]] = None): Boolean = {
        val modelPath: Path = Paths.get("resources", "output","model")
        if (!Files.exists(modelPath)) Files.createDirectory(modelPath)
        val pmmlFile: Path = modelPath.resolve(
          Settings.Spark.Model.file.replaceAll("score", (100 * score).toInt.toString))
        val modelWriter: BufferedWriter = Files.newBufferedWriter(pmmlFile, StandardCharsets.UTF_8,
          StandardOpenOption.WRITE,
          StandardOpenOption.CREATE,
          StandardOpenOption.TRUNCATE_EXISTING)
        val pmml: PMML = pipelineModel.toPMML(schema, verificationData)
        pmml.addExtensions()
        val writeOp = scala.util.Try(JAXBUtil.marshalPMML(pmml, new StreamResult(modelWriter))).toOption
        modelWriter.close()
        writeOp.nonEmpty
      }
    }
  }

  object Regression {
    val evaluator: RegressionEvaluator = new RegressionEvaluator()
      .setLabelCol("target")
      .setPredictionCol("prediction")
      .setMetricName("rmse")

    final case class GridSearch(
                         processing: Pipeline,
                         crossValidationFolds: Int = 1,
                         parallelism: Int = 1
                         ) {
      val basePipeline: Pipeline = new Pipeline()
      // ML Regression Models
      val randomForest: RandomForest = new RandomForest(basePipeline, processing)
      // Parameter Grid
      val paramGrid: Array[ParamMap] = randomForest.paramGrid
      // Cross Validation
      val crossValidator: CrossValidator = new CrossValidator()
        .setEstimator(basePipeline)
        .setEvaluator(evaluator)
        .setEstimatorParamMaps(paramGrid)
        .setNumFolds(crossValidationFolds)
        .setParallelism(parallelism)

      def fit[A](train: Dataset[A]): PipelineModel =
        crossValidator.fit(train).bestModel.asInstanceOf[PipelineModel]
    }
  }

}
