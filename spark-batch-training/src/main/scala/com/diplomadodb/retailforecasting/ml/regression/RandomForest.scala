package com.diplomadodb.retailforecasting.ml.regression

import org.apache.spark.ml.Pipeline
import org.apache.spark.ml.param.ParamMap
import org.apache.spark.ml.regression.RandomForestRegressor
import org.apache.spark.ml.tuning.ParamGridBuilder

class RandomForest(basePipeline: Pipeline, processing: Pipeline) {
  import com.diplomadodb.retailforecasting.config.Settings.Spark.Model.Regression.RandomForest._

  private val rf: RandomForestRegressor = new RandomForestRegressor()
    .setLabelCol("target")
    .setFeaturesCol("features")
    .setMaxBins(400)

  val paramGrid: Array[ParamMap] = new ParamGridBuilder()
    .baseOn(basePipeline.stages -> (processing.getStages :+ rf))
    .addGrid(rf.numTrees, numTrees)
    .addGrid(rf.maxDepth, maxDepth)
    .build()
}
