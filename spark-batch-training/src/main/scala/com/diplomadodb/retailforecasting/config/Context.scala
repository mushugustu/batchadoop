package com.diplomadodb.retailforecasting.config

import org.apache.spark.sql.SparkSession

trait Context {
  import Settings.Spark._
  val spark: SparkSession = SparkSession.builder
    .appName(name)
    .config(Tags.Label.master, Tags.Value.master)
    .config(Tags.Label.driverMem, Tags.Value.driverMem)
    .config(Tags.Label.executorMem, Tags.Value.executorMem)
    .getOrCreate()
}
