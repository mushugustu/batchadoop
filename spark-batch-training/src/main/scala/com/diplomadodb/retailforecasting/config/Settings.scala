package com.diplomadodb.retailforecasting.config

import com.typesafe.config.{Config, ConfigFactory}
import collection.JavaConversions._

object Settings {
  private val app: Config = ConfigFactory.load().getConfig("application")
  object Spark {
    private val spark: Config = app.getConfig("spark")
    val name: String = spark.getString("name")
    object Tags {
      private val tags: Config = spark.getConfig("tags")
      object Label {
        private val label: Config = tags.getConfig("label")
        val master: String        = label.getString("master")
        val executorMem: String   = label.getString("executorMem")
        val driverMem: String     = label.getString("driverMem")
      }
      object Value {
        private val value: Config = tags.getConfig("value")
        val master: String        = value.getString("master")
        val executorMem: String   = value.getString("executorMem")
        val driverMem: String     = value.getString("driverMem")
      }
    }
    object Model {
      private val model: Config = spark.getConfig("model")
      val file: String = model.getString("file")
      val cvFold: Int = model.getInt("cvFold")
      val parallelism: Int = model.getInt("parallelism")
      object Regression {
        private val regression: Config = model.getConfig("regression")
        object RandomForest {
          private val randomForest: Config = regression.getConfig("randomForest")
          val numTrees: Array[Int] = randomForest.getIntList("numTrees").toList.map(_.toInt).toArray
          val maxDepth: Array[Int] = randomForest.getIntList("maxDepth").toList.map(_.toInt).toArray
        }
      }
    }
  }
  object Data {
    private val data: Config = app.getConfig("data")
    val input: String = data.getString("input")
    val predictions: String = data.getString("predictions")
  }
}
