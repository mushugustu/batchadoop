package com.diplomadodb.retailforecasting

import com.diplomadodb.retailforecasting.config.{Context, Settings}
import com.diplomadodb.retailforecasting.data.DataProcessing
import com.diplomadodb.retailforecasting.ml.Model
import org.apache.spark.ml.PipelineModel
import org.apache.spark.sql.{Dataset, Row}

object Application extends Context {
  import com.diplomadodb.retailforecasting.config.Settings.Spark.Model._
  import Settings.Spark.Model._
  import Model.Implicits._

  // Get best model using grid-search and cross-validation
  val model: PipelineModel = Model.Regression.GridSearch(DataProcessing.pipeline, cvFold, parallelism)
    .fit(DataProcessing.train.repartition(1).cache())

  // Generate predictions for the train and test dataset
  val predTrain: Dataset[Row] = model.transform(DataProcessing.train)
  val predTest: Dataset[Row]  = model.transform(DataProcessing.test)

  // Generate rmse for train and test predictions
  val rmseTrain: Double = Model.Regression.evaluator evaluate predTrain
  val rmseTest: Double  = Model.Regression.evaluator evaluate predTest

  // Generate predictions and rmse for the complete dataset
  val predictions: Dataset[Row] = model.transform(DataProcessing.rawData.repartition(1))
  val rmse: Double = Model.Regression.evaluator evaluate predictions

  def main(args: Array[String]): Unit = {
    // Save predictions
    predictions
      .select("sku", "sku_index", "position", "lag3", "lag2", "lag1" ,
        "prevCumulativeDemand", "actualCumulativeDemand", "target", "prediction")
      .coalesce(1).write.option("header", value = true).csv(Settings.Data.predictions)

    // Save the model
    val modelExported: Boolean = model.exportPMML(
      DataProcessing.rawData.schema, rmseTest, None)

    spark.close()
  }

}
