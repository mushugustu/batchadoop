package com.diplomadodb.retailforecasting.model

sealed trait Week

object Week {

  final case class OfYear(year: Int, position: Int, demand: Double) extends Week {
    def add(value: Double): OfYear = OfYear(year, position, demand + value)

    def toRelative(referenceYear: Int): Relative=
      Relative(48 * (year - referenceYear) + position, demand)
  }

  final case class Relative(position: Int, demand: Double) extends Week {
    def add(value: Double): Relative = Relative(position, demand + value)
    def -(other: Relative): Relative = Relative(position, demand - other.demand)
    def `return`(other: Relative): Return = Return(position, other.demand, demand)
  }

  final case class Return(position: Int, before: Double, now: Double) extends Week {
    val value: Double = now / before - 1
  }
}
