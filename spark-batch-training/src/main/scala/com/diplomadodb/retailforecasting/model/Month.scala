package com.diplomadodb.retailforecasting.model

sealed trait Month {
  def d: String
  def y: String
  def week: Int = (m + 1) * 4
  def demand: Int  = {
    val temp = scala.util.Try(d.replace(",", "").toInt).toOption.getOrElse(0)
    if (temp > 0) temp else 0
  }
  def year: Int    = scala.util.Try(y.replace(",", "").toInt).toOption.getOrElse(0)
  def m: Int = this match {
    case month: Month.Jan => 0
    case month: Month.Feb => 1
    case month: Month.Mar => 2
    case month: Month.Apr => 3
    case month: Month.May => 4
    case month: Month.Jun => 5
    case month: Month.Jul => 6
    case month: Month.Aug => 7
    case month: Month.Sep => 8
    case month: Month.Oct => 9
    case month: Month.Nov => 10
    case month: Month.Dec => 11
  }
  override def toString: String = this match {
    case Month.Jan(_, _) => "january"
    case Month.Feb(_, _) => "february"
    case Month.Mar(_, _) => "march"
    case Month.Apr(_, _) => "april"
    case Month.May(_, _) => "may"
    case Month.Jun(_, _) => "june"
    case Month.Jul(_, _) => "july"
    case Month.Aug(_, _) => "august"
    case Month.Sep(_, _) => "september"
    case Month.Oct(_, _) => "october"
    case Month.Nov(_, _) => "november"
    case Month.Dec(_, _) => "december"
  }
}
object Month {
  final case class Jan(d: String, y: String) extends Month
  final case class Feb(d: String, y: String) extends Month
  final case class Mar(d: String, y: String) extends Month
  final case class Apr(d: String, y: String) extends Month
  final case class May(d: String, y: String) extends Month
  final case class Jun(d: String, y: String) extends Month
  final case class Jul(d: String, y: String) extends Month
  final case class Aug(d: String, y: String) extends Month
  final case class Sep(d: String, y: String) extends Month
  final case class Oct(d: String, y: String) extends Month
  final case class Nov(d: String, y: String) extends Month
  final case class Dec(d: String, y: String) extends Month
}