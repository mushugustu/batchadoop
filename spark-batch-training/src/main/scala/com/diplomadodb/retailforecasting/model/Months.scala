package com.diplomadodb.retailforecasting.model

final case class Months(
                         jan: Month.Jan,
                         feb: Month.Feb,
                         mar: Month.Mar,
                         apr: Month.Apr,
                         may: Month.May,
                         jun: Month.Jun,
                         jul: Month.Jul,
                         aug: Month.Aug,
                         sep: Month.Sep,
                         oct: Month.Oct,
                         nov: Month.Nov,
                         dec: Month.Dec) {
  def toList: List[Month] = List(jan, feb, mar, apr, may, jun, jul, aug, sep, oct, nov, dec)
  val toWeeks: List[Week.OfYear] = toList map { month => Week.OfYear(month.year, month.week, month.demand)}
}


