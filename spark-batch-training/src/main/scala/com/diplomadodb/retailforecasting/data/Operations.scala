package com.diplomadodb.retailforecasting.data

import com.diplomadodb.retailforecasting.model.Week
import breeze.interpolation._
import breeze.linalg.DenseVector

object Operations {

  def createSplinesRight(values: List[Week.Relative]): List[Week.Relative] = values match {
    case List(w1, w2, w3) =>
      val x = DenseVector(w1.position.toDouble, w2.position.toDouble, w3.position.toDouble)
      val y = DenseVector(w1.demand, w2.demand, w3.demand)
      val f = CubicInterpolator(x, y)
      val g = (x: Double) => {
        val yhat = f(x)
        if (yhat > 0) yhat else 0
      }
      val splines: List[Week.Relative] = ((w1.position + 1) until w2.position).toList.map(t => Week.Relative(t, g(t)))
      w1 +: splines
  }
  def createSplinesLeft(values: List[Week.Relative]): List[Week.Relative] = values match {
    case List(w1, w2, w3) =>
      val x = DenseVector(w1.position.toDouble, w2.position.toDouble, w3.position.toDouble)
      val y = DenseVector(w1.demand, w2.demand, w3.demand)
      val f = CubicInterpolator(x, y)
      val g = (x: Double) => {
        val yhat = f(x)
        if (yhat > 0) yhat else 0
      }
      val splines: List[Week.Relative] = ((w2.position + 1) until w3.position).toList.map(t => Week.Relative(t, g(t)))
      w2 +: splines :+ w3
  }

  def fixDecreasingDemand(ls: List[Week.Relative]): List[Week.Relative] =
    ls.tail.foldLeft(List(ls.head))((fixed, d) => if (fixed.last.demand > d.demand) fixed :+ fixed.last else fixed :+ d)
}
