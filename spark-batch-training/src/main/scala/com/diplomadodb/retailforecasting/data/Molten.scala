package com.diplomadodb.retailforecasting.data

import com.diplomadodb.retailforecasting.config.{Context, Settings}
import com.diplomadodb.retailforecasting.model._
import org.apache.spark.sql._

object Molten extends Context {
  // See http://r4ds.had.co.nz/tidy.html for more information about the nature of this data transformation
  // Raw/Messy Data => Molten/Tidy Data

  import spark.implicits._

  val readPath: String = Settings.Data.input
  val rawData: Dataset[Model.Raw] = spark.read.option("header", value=true).csv(readPath).repartition(1)
    .map(row => Model.Raw(row.getString(0), Months(
      Month.Jan(row.getString(2), row.getString(1)),
      Month.Feb(row.getString(3), row.getString(1)),
      Month.Mar(row.getString(4), row.getString(1)),
      Month.Apr(row.getString(5), row.getString(1)),
      Month.May(row.getString(6), row.getString(1)),
      Month.Jun(row.getString(7), row.getString(1)),
      Month.Jul(row.getString(8), row.getString(1)),
      Month.Aug(row.getString(9), row.getString(1)),
      Month.Sep(row.getString(10), row.getString(1)),
      Month.Oct(row.getString(11), row.getString(1)),
      Month.Nov(row.getString(12), row.getString(1)),
      Month.Dec(row.getString(13), row.getString(1))
    )))

  val data: Dataset[Model.Datapoint] = rawData.groupByKey(raw => raw.sku)
    .mapGroups({case (key, values) => Model.Grouped(key, values.map(_.months).toList)})
    .flatMap(_.flatten)
    .cache()

  object Model {

    final case class Raw(sku: String, months: Months)

    final case class Sample(sku: String, position: Int, r: Double)
    final case class Datapoint(sku: String, position: Int, lag3: Double, lag2: Double, lag1: Double, target: Double,
                               prevCumulativeDemand: Double, actualCumulativeDemand: Double) {
      def isInvalid: Boolean = lag3.isNaN || lag2.isNaN || lag3.isNaN || target.isNaN
      def isValid: Boolean = ! isInvalid
    }

    final case class Grouped(sku: String, grouped: List[Months]) {
      val relativeWeeks: List[Week.OfYear] = grouped.flatMap(_.toWeeks).sortWith(
        (w1: Week.OfYear, w2: Week.OfYear) => (100 * w2.year + w2.position) > (100 * w1.year + w1.position))
      val absoluteWeeks: List[Week.Relative] = {
        val referenceYear = relativeWeeks.map(_.year).foldLeft(Int.MaxValue)((min, x) => if (x < min) x else min)
        relativeWeeks map (_.toRelative(referenceYear))
      }
      val cumulativeWeeks: List[Week.Relative] =
        absoluteWeeks.tail.foldLeft(List(absoluteWeeks.head))((weeks, week) => weeks :+ week.add(weeks.last.demand))

      val sliding: List[List[Week.Relative]] = cumulativeWeeks.sliding(3).toList
      val allCumulativeWeeks: List[Week.Relative] =
        Operations.fixDecreasingDemand(sliding.flatMap(Operations.createSplinesRight) ++ Operations.createSplinesLeft(sliding.last))
      val weeks: List[Week.Relative] = allCumulativeWeeks.init zip allCumulativeWeeks.tail map {case (init, tail) => tail - init}
      val returns: List[Week.Return] = allCumulativeWeeks.init zip allCumulativeWeeks.tail map {case (init, tail) => tail `return` init}

      def flatten: List[Datapoint] = returns.sliding(4).toList.map({
        case List(lag3, lag2, lag1, target) =>
          Datapoint(sku, target.position, lag3.value, lag2.value, lag1.value, target.value, target.before, target.now)
      }).filter(_.isValid)
    }
  }
}
