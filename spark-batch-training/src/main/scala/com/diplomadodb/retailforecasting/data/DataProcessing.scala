package com.diplomadodb.retailforecasting.data

import com.diplomadodb.retailforecasting.config.Context
import org.apache.spark.ml.{Pipeline, PipelineStage}
import org.apache.spark.ml.feature.{StringIndexer, StringIndexerModel, VectorAssembler}
import org.apache.spark.sql.Dataset
import org.apache.spark.sql.functions.rand

object DataProcessing extends Context {
  val rawData: Dataset[Molten.Model.Datapoint] = Molten.data.orderBy(rand()).cache() 
  val Array(train, test) = rawData.randomSplit(Array(0.7, 0.3))
  val stages: Array[PipelineStage] = Features.stages
  object Features {
    val cols = Array("lag3", "lag2", "lag1", "sku_index")
    val featureIndexer: StringIndexerModel  = new StringIndexer()
      .setInputCol("sku")
      .setOutputCol("sku_index")
      .fit(rawData)
    val vectorAssembler: VectorAssembler    = new VectorAssembler()
      .setInputCols(cols)
      .setOutputCol("features")
    val stages: Array[PipelineStage] = Array(featureIndexer, vectorAssembler)
  }
  val pipeline: Pipeline = new Pipeline().setStages(stages)
}
