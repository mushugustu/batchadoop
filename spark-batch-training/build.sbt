import Dependencies._

lazy val root = (project in file(".")).
  settings(
    inThisBuild(List(
      organization := "com.diplomadodb",
  scalaVersion := "2.11.8",
  version      := "0.0.0"
  )),
  name := "spark-batch-training",
  libraryDependencies ++= {
    val configVersion = "1.3.1"
    val sparkVersion  = "2.3.0"
    val jpmmlSparkML  = "1.4.5"
    val breezeVersion = "0.13.2"
    Seq(
      "com.typesafe" % "config" % configVersion,
      // Spark
      "org.apache.spark" %% "spark-core"  % sparkVersion,
      "org.apache.spark" %% "spark-sql"   % sparkVersion,
      "org.apache.spark" %% "spark-mllib" % sparkVersion,
      // Breeze
      "org.scalanlp" %% "breeze" % breezeVersion,
      // PMML
      "org.jpmml" % "jpmml-sparkml" % jpmmlSparkML,
      // Logging
      "ch.qos.logback" % "logback-classic" % "1.2.3",
      "com.typesafe.scala-logging" %% "scala-logging" % "3.9.0",
      // Test
      scalaTest % Test
    )
  },
    assemblyMergeStrategy in assembly := {
      case PathList("META-INF", xs @ _*) => MergeStrategy.discard
      case x => MergeStrategy.first
    }
  )
