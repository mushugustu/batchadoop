#Reference
#https://hub.docker.com/r/wurstmeister/kafka/

#Follow the guide to install docker compose
https://docs.docker.com/compose/install/

#Install the git repository
git clone https://github.com/wurstmeister/kafka-docker.git

#Edit file docker-compose-single-broker.yml replace localhost instead of 192.168.99.100
#Also delete the line with the text KAFKA_CREATE_TOPICS: "test:1:1" 
nano docker-compose-single-broker.yml

#Run locally kafka
docker-compose -f docker-compose-single-broker.yml up -d

#Download Kafka client. 
#Select a suitable folder to execute this command
#Maybe your home directory, type 
cd ~
#If using windows this is the page to download
#https://www.apache.org/dyn/closer.cgi?path=/kafka/2.0.0/kafka_2.11-2.0.0.tgz

wget 'https://www-eu.apache.org/dist/kafka/2.0.0/kafka_2.11-2.0.0.tgz'
tar -xzf kafka_2.11-2.0.0.tgz
rm kafka_2.11-2.0.0.tgz 

#Go to the kafka directory
cd kafka_2.11-2.0.0

#Follow the instructions of kafka from step 3
#https://kafka.apache.org/quickstart

