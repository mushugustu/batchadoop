package com.diplomadobd.streaming;


import java.nio.ByteBuffer;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;

// create the topic
// bin/kafka-topics.sh --create --zookeeper localhost:2181 --replication-factor 1 --partitions 1 --topic clusterdb-topic1

public class MongoDBSimpleConsumer {
    public static void main(String args[]) {
        MongoDBSimpleConsumer example = new MongoDBSimpleConsumer();
        //long maxReads = Long.parseLong(args[0]);
        long maxReads = 100;
        //String topic = args[1];
        //String topic = args[1];
        String topic = "numbers";
        //int partition = Integer.parseInt(args[2]);
        int partition = 0;
        List<String> seeds = new ArrayList<String>();
        //seeds.add(args[3]);
        seeds.add("127.0.0.1");
        //int port = Integer.parseInt(args[4]);
        int port = 9092;
        try {
            example.run(maxReads, topic, partition, seeds, port);
        } catch (Exception e) {
            System.out.println("Oops:" + e);
            e.printStackTrace();
        }
    }

    private List<String> m_replicaBrokers = new ArrayList<String>();

    public MongoDBSimpleConsumer() {
        m_replicaBrokers = new ArrayList<String>();
    }

    public void run(long a_maxReads, String a_topic, int a_partition,
        List<String> a_seedBrokers, int a_port) throws Exception {
        // find the meta data about the topic and partition we are interested in

        String clientName = "Client_" + a_topic + "_" + a_partition;


        Properties props = new Properties();
        props.put("bootstrap.servers", "localhost:9092");
        props.put("group.id", "gid_numbers_0");
        props.put("enable.auto.commit", "true");
        props.put("auto.commit.interval.ms", "1000");
        props.put("key.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
        props.put("value.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");

        KafkaConsumer<String, String> consumer = new KafkaConsumer<>(props);
        consumer.subscribe(Arrays.asList("numbers"));

        while (true) {
            ConsumerRecords<String, String> records = consumer.poll(100);
            for (ConsumerRecord<String, String> record : records)
                System.out.printf("offset = %d, key = %s, value = %s%n", record.offset(), record.key(), record.value());
        }

                    // We asked for an invalid offset. For simple case ask
                    // for the last element to reset


            // run the kafka server, create the topic. 
            // Create the mongodb database
            // Create the collection
            // Populate the fish data. 
            // show dbs
            // use clusterdb
            // collection: db.createCollection("fish")
            // cd("/home/curso/Desktop/StreamingData")
            // mongoimport --db clusterdb --collection fish --type json --file Fish.json (from shell)
            // db.fish.find()
            // The data comes from the message: load the messages first. 
            // delete  db.fish.remove({})
            // Step 4: Send some messages ok, see how to load a file, delete the data from db. 
            // add a single message: {"internationalFishId": 93734195, "name": "Gerald", "breed": "Turbot"}
            // bin/kafka-console-producer.sh --broker-list localhost:9092 --topic clusterdb-topic1
            // {"internationalFishId": 93734195, "name": "Gerald", "breed": "Turbot"}

            // bin/kafka-consumer-offset-checker --topic clusterdb-topic1 --zookeeper zk01.example.com:2181

            // bin/kafka-topics.sh --zookeeper localhost:2181 --list

            // Consumer
            // bin/kafka-console-consumer.sh --zookeeper localhost:2181 --topic clusterdb-topic1 --from-beginning

            // delete: bin/kafka-run-class.sh kafka.admin.TopicCommand --delete --topic clusterdb-topic1 --zookeeper localhost:2181

          /*  MongoClient client = new MongoClient();
            MongoDatabase db = client.getDatabase("clusterdb");
            MongoCollection<Document> fishCollection = db.getCollection("fish");
            Gson gson = new Gson();
            Type type = new TypeToken<Fish>() {}.getType(); */

                //System.out.println(incomingFish);
                //fishCollection.insertOne(incomingFish.getFishAsDocument());
    }


}