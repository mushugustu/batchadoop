/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.diplomadobd.retailda;

import com.diplomadobd.dataintegration.io.core.TextFileHelper;
import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.util.List;
import java.util.Scanner;
import com.diplomadobd.dao.RetailDAO;

/**
 *
 * @author mario
 */
public class KnowledgeFlowGen {
    private String resourceFolder;
    private String kfFolder;
    private String kfTemplateFile;
    private String abtFolder;
    private String modelFolder;
    private Connection conn ;

    public KnowledgeFlowGen(String resourceFolder, String kfFolder, String kfTemplateFile,String abtFolder, String modelFolder,Connection conn){

        this.resourceFolder = resourceFolder;
        boolean flag = new File(resourceFolder).mkdirs();
        this.kfFolder = kfFolder;
        flag = new File(resourceFolder+"/"+kfFolder).mkdirs();
        this.kfTemplateFile = kfTemplateFile;
        this.abtFolder = abtFolder;
        flag = new File(resourceFolder+"/"+abtFolder).mkdirs();
        this.modelFolder = modelFolder;
        flag = new File(resourceFolder+"/"+modelFolder).mkdirs();
        this.conn = conn;
    }
    public void generateKF(int cumRule){
        //Read the template kf
        try{
        String template = new Scanner(new File(this.kfTemplateFile)).useDelimiter("\\Z").next();
        List<String> theSkus = RetailDAO.getSkus(conn);
        for (String theSku : theSkus) {
            String curContent = new String(template);
            String token = theSku+"-"+cumRule;
            String kfFileName = this.resourceFolder+"/"+this.kfFolder+"/"+token+".kf";
            curContent = curContent.replace("template", token+".csv")
            .replace("resourceFolder", this.resourceFolder)
            .replace("modelFolder", this.modelFolder)
            .replace("abtFolder", this.abtFolder);
            BufferedWriter bw = TextFileHelper.getBufferedWriter(kfFileName);
            bw.write(curContent);
            bw.close();
        }
        }catch (IOException ex){
            throw new IllegalStateException("Error reading the template");
        }
    }
}
