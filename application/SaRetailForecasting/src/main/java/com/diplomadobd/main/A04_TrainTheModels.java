/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.diplomadobd.main;


import java.io.BufferedReader;
import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.util.List;

import com.diplomadobd.aux.CodeControl;
import com.diplomadobd.dao.Dao;
import com.diplomadobd.dao.RetailDAO;

import weka.knowledgeflow.Flow;
import weka.knowledgeflow.FlowRunner;
import weka.knowledgeflow.steps.Job;
import weka.knowledgeflow.steps.Step;

/**
 *
 * @author mario
 */
public class A04_TrainTheModels {
    
    public static void main(String[] args) {
        String workingDir ="/Users/mg/Documents/workingdir";
        if(args[0]!=null && args[0].length()>0){
            workingDir =args[0];
            if(workingDir.charAt(workingDir.length()-1)=='/'){
                workingDir = workingDir.substring(0,workingDir.length()-1);
            }
        }
        String kfFolder = workingDir+"/kflows";

        Integer [] cumRule = new Integer[3];
        cumRule[0]=1;
        cumRule[1]=2;
        cumRule[2]=3;
        Connection conn = null;
        CodeControl control = new CodeControl();
        try {
            control.disableSystemExit();

            //Init Weka objects

            args = new String[1];
            //Get the skus
            Dao dao = new Dao();
            conn = dao.getDbConnection();
            List<String> skus = RetailDAO.getSkus(conn);
            //Cycle of several KFs
            for (String sku : skus) {
                for(Integer theRule:cumRule){
                    FlowRunner fr = new FlowRunner();
                    String token = sku + "-" + theRule + ".kf";
                    args[0] = kfFolder+"/"+token;
                    try {
                        fr.run(fr, args);
                    }catch(IllegalStateException ex){
                        //Do nothing.
                    }
                }
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            control.enableSystemExit();
        }
    }
    
}
