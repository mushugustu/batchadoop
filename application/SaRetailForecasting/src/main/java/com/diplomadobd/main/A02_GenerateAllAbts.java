/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.diplomadobd.main;

import java.sql.Connection;
import java.util.Map;
import com.diplomadobd.dao.Dao;
import com.diplomadobd.dao.RetailDAO;
import com.diplomadobd.dto.SplineAccumRule;
import com.diplomadobd.retailda.DataAsFunction;
import com.diplomadobd.retailda.main.GenerateABT;

/**
 *
 * @author mario
 */
public class A02_GenerateAllAbts {

    private Connection conn;

    public A02_GenerateAllAbts() {
        //Get the Connection
        this.conn = new Dao().getDbConnection();

    }

    /**
     * Generate all steps from the original data
     *
     * @param rules When Rules is null execute all the the rules. Else use the
     * rules in the array
     */
    public void generateFromOriginalData(int baseYear, Integer[] rules, String abtPath) {
        int start, end;
        //Generate Splines and the augmented table
        DataAsFunction dataAsaF = new DataAsFunction();
        dataAsaF.generateSynthDemand(baseYear, conn);
        //Get all the rules above zero
        Map<Integer, SplineAccumRule> theRules = RetailDAO.getRulesAboveZero(conn);
        if (rules == null) {
            rules = theRules.keySet().toArray(rules);
        }
        
        
        //Create other objects
        GenerateABT gABT = new GenerateABT();
        for (Integer ruleId : rules) {
            SplineAccumRule rule = theRules.get(ruleId);
            if (rule != null) {                
                //Generar las tablas ABT, por cada Cum rule
                gABT.generateAbt(ruleId, conn, abtPath);
                //Calcular los modelos
                //Hacer la predicción del Recall
                //Hacer la predicción de los siguientes valores consecutivos               

            } else {
                throw new IllegalStateException("The idcum " + ruleId + " does not exist.");
            }
        }
    }
    /**
     * Main method to generate all from scratch
     * @param args 
     */
    public static void main(String[] args) {
        A02_GenerateAllAbts gF = new A02_GenerateAllAbts();
        String workingDir ="/Users/mg/Documents/workingdir";
        if(args[0]!=null && args[0].length()>0){
            workingDir =args[0];
            if(workingDir.charAt(workingDir.length()-1)=='/'){
                workingDir = workingDir.substring(0,workingDir.length()-1);
            }
        }
        String abtFolder = workingDir+"/datasets/abt";

        //null means generate all rules
        Integer[] rules = new Integer[3];
        rules[0] = 1;
        rules[1] = 2;
        rules[2] = 3;
        gF.generateFromOriginalData(2014, rules, abtFolder);
    }

}
