/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.diplomadobd.main;

import java.sql.Connection;
import com.diplomadobd.dao.Dao;
import com.diplomadobd.retailda.KnowledgeFlowGen;

/**
 *
 * @author mario
 */
public class A03_GenerateKFs {

    public static void main(String[] args) {
        Integer[] cumRules = new Integer[3];
        cumRules[0] = 1;
        cumRules[1] = 2;
        cumRules[2] = 3;
        Connection conn = null;
        try {
            String resourceFolder ="/Users/mg/Documents/workingdir";
            if(args[0]!=null && args[0].length()>0){
                resourceFolder =args[0];
                if(resourceFolder.charAt(resourceFolder.length()-1)=='/'){
                    resourceFolder = resourceFolder.substring(0,resourceFolder.length()-1);
                }
            }
            Dao dao = new Dao();
            conn = dao.getDbConnection();
            String abtFolder = "datasets/abt";
            String kfFolder = "kflows";
            String modelFolder ="models";
            String kflTtemplateFileName = "src/main/resources/kfl/template.kf";
            KnowledgeFlowGen kf = new KnowledgeFlowGen(resourceFolder, kfFolder, kflTtemplateFileName
                , abtFolder, modelFolder, conn);
            for (Integer cumRule:cumRules) {
                kf.generateKF(cumRule);
            }
            System.out.println("OK");
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                conn.close();
            } catch (Exception ex) {
                //Do Nothing
            }
        }
    }
}
