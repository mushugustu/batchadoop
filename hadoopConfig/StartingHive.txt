#start and stop Derby server for metastore
~/startNetworkServer -h 0.0.0.0 &
#Verify the metastore is up



#In another console start the server hive2 to connect via jdbc
hive --service hiveserver2 --hiveconf hive.server2.thrift.port=10000 --hiveconf hive.root.logger=INFO,console

#Stop hive metastore
stopNetworkServer
