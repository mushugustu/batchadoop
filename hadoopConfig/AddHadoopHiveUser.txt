#Create a user
sudo adduser –ingroup
#Add permissions to other users using the administrator account
hdfs dfs –chmod -R 1777 /tmp
#In user machine, give write access to hadoop temp directory-
#Now, give write permission to the user group on hadoop.tmp.dir.To get the path for hadoop.tmp.dir open core-site.xml ., And, this should be done only in the node (Machine), where the new user is added.
chmod 777 /tmp/hadoop-hduser
chmod 777 -R /tmp/hive

#[4] Now Create user home directory in HDFS-
#For New user create a directory structure in HDFS.
 hadoop dfs –mkdir /user/username/
# [5] In HDFS,change the ownership of user home directory
#Superuser has the ownership of newly created directory structure.But the new user will not be able to run MapReduce programs with this. So, to achieve this, change the ownership of newly created directory in HDFS to the new user.
hadoop dfs –chown –R username:groupname /user/username/

