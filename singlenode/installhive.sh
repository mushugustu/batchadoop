#Removes the Path variable
head -n -1 /etc/profile.d/bigdata_env.sh >tmp.txt
sudo mv tmp.txt /etc/profile.d/bigdata_env.sh

#Get Hive 3.1.0
wget http://www-us.apache.org/dist/hive/hive-3.1.0/apache-hive-3.1.0-bin.tar.gz
#Unzip Hive
tar -xzf apache-hive-3.1.0-bin.tar.gz
sudo mv  apache-hive-3.1.0-bin /opt/hive
#Set privileges
#change the permissions to the hduser user.
sudo chown -R hduser:hduser hive
#Set privileges
#change the permissions to the hduser user.
sudo chown -R hdadmin:hdadmin /opt/hive
#Remove file downloaded
rm apache-hive-3.1.0-bin.tar.gz
#Copy conf files
cp gitviews/batchadoop/hadoopConfig/hiveconf/* /opt/hive/conf/
#Add env variables
echo  "export HIVE_HOME=/opt/hive/apache-hive-3.1.0-bin" | sudo tee --append /etc/profile.d/bigdata_env.sh
export HIVE_HOME=/opt/hive
#Download Derby for the Hive metastore
wget http://www-eu.apache.org/dist//db/derby/db-derby-10.14.2.0/db-derby-10.14.2.0-bin.zip
#Install uzip
sudo apt-get install unzip
unzip db-derby-10.14.2.0-bin.zip
sudo mv db-derby-10.14.2.0-bin /opt/derby
sudo chown -R hdadmin:hdadmin /opt/derby
#Delete original zip file
rm db-derby-10.14.2.0-bin.zip
#Create env variables for DERBY_HOME
echo  "export DERBY_HOME=/opt/derby/db-derby-10.14.2.0-bin" | sudo tee --append /etc/profile.d/bigdata_env.sh
export DERBY_HOME=/opt/derby/db-derby-10.14.2.0-bin
#Create folder for hive metastore (database)
mkdir $DERBY_HOME/data
#Create folders in hdfs for Hive
starthadoop.sh
hdfs dfs -ls
dfs dfs -mkdir /user/hive/
hdfs dfs -mkdir /user/hive/warehouse
hdfs dfs -chmod g+w /tmp
hdfs dfs -chmod g+w /user/hive/warehouse
#Now start Derby
pushd $DERBY_HOME/data
$DERBY_HOME/bin/startNetworkServer -h 0.0.0.0 &
#Run the first time script to configure the Database
$HIVE_HOME/bin/schematool -initSchema -dbType derby
#Verify the hive_installation
$HIVE_HOME/bin/hive --service metastore &
#shutdown derby
$DERBY_HOME/bin/stopNetworkServer
popd
stophadoop.sh




source /etc/profile.d/bigdata_env.sh
echo  "export CLASSPATH=$CLASSPATH:$DERBY_HOME/lib/derby.jar:$DERBY_HOME/lib/derbytools.jar" | sudo tee --append /etc/profile.d/bigdata_env.sh
export PATH=$PATH:$DERBY_HOME/bin
echo  "export PATH=$PATH:$HIVE_HOME/bin:$DERBY_HOME/bin" | sudo tee --append /etc/profile.d/bigdata_env.sh
export CLASSPATH=$CLASSPATH:$DERBY_HOME/lib/derby.jar:$DERBY_HOME/lib/derbytools.jar
